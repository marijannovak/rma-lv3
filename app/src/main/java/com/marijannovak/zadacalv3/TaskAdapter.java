package com.marijannovak.zadacalv3;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by marij on 5.4.2017..
 */

public class TaskAdapter extends BaseAdapter {

    ArrayList<Task> taskList;

    public TaskAdapter(ArrayList<Task> list) {

        this.taskList = list;

    }

    @Override
    public int getCount() {
        return taskList.size();
    }

    @Override
    public Object getItem(int position) {
        return taskList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        int drawable = R.drawable.nothing;
        int color = Color.argb(100, 0 , 0 ,0);

        if(convertView == null)
        {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.item_task, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }

        else
        {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Task task = this.taskList.get(position);
        viewHolder.taskTitle.setText(task.getTaskTitle());
        viewHolder.taskDescription.setText(task.getTaskDescription());
        viewHolder.taskCategory.setText(String.valueOf(task.getTaskCategory()));

        if(task.getTaskCategory().equals("College work")) drawable = R.drawable.fakultet;
        else if(task.getTaskCategory().equals("Housework")) drawable = R.drawable.spremanje;
        else if(task.getTaskCategory().equals("Job")) drawable = R.drawable.job;
        else if(task.getTaskCategory().equals("Free time")) drawable = R.drawable.hobi;

        Picasso.with(parent.getContext())
                .load(drawable)
                .fit()
                .centerCrop()
                .into(viewHolder.taskIcon);

       if(parent.getChildAt(position) != null)  {

            if (task.getPriorityColor().equals("Red"))
                color = Color.parseColor("#f44242");
            else if (task.getPriorityColor().equals("Yellow"))
                color = Color.parseColor("#f4d941");
            else if (task.getPriorityColor().equals("Blue"))
                color = Color.parseColor("#42b0f4");


           parent.getChildAt(position).setBackgroundColor(color);

       }


        return convertView;
    }


    public void removeAt(int position) {

        this.taskList.remove(position);
        this.notifyDataSetChanged();

    }



    private static class ViewHolder{

        TextView taskTitle, taskDescription, taskCategory;
        ImageView taskIcon;

        public ViewHolder(View taskView)
        {
            taskTitle = (TextView) taskView.findViewById(R.id.tvTitle);
            taskDescription = (TextView) taskView.findViewById(R.id.tvDescription);
            taskCategory = (TextView) taskView.findViewById(R.id.tvCategory);
            taskIcon = (ImageView) taskView.findViewById(R.id.ivIcon);
        }

    }

}
