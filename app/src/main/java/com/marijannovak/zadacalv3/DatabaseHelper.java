package com.marijannovak.zadacalv3;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by marij on 5.4.2017..
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static DatabaseHelper Instance = null;

    private DatabaseHelper(Context context)
    {
        super(context.getApplicationContext(), Schema.DATABASE_NAME, null, Schema.SCHEMA_VERSION);

    }

    public static synchronized DatabaseHelper getInstance(Context context)
    {
        if(Instance == null)
        {
            Instance = new DatabaseHelper(context);
        }

        return Instance;
    }


    static final String CREATE_TABLE_TASKS = "CREATE TABLE " + Schema.TABLE_TASKS +
            " (" + Schema.TITLE + " TEXT," + Schema.DESCRIPTION + " TEXT," + Schema.CATEGORY + " TEXT,"  + Schema.COLOR + " TEXT);";

    static final String CREATE_TABLE_CATEGORIES = "CREATE TABLE " + Schema.TABLE_CATEGORIES + " (" +Schema.CATEGORY + " TEXT);";


    static final String DROP_TABLE_TASKS = "DROP TABLE IF EXISTS " + Schema.TABLE_TASKS;

    static final String DROP_TABLE_CATEGORIES = "DROP TABLE IF EXISTS " + Schema.TABLE_CATEGORIES;


    static final String SELECT_ALL_TASKS = "SELECT " + Schema.TITLE + "," + Schema.DESCRIPTION + ","
            + Schema.CATEGORY + "," + Schema.COLOR + " FROM " + Schema.TABLE_TASKS;

    static final String SELECT_ALL_CATEGORIES = "SELECT " + Schema.CATEGORY + " FROM " + Schema.TABLE_CATEGORIES;

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_TABLE_TASKS);
        db.execSQL(CREATE_TABLE_CATEGORIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE_TASKS);
        db.execSQL(DROP_TABLE_CATEGORIES);

        this.onCreate(db);
    }

    public void insertTask(Task task){

        ContentValues contentValues = new ContentValues();
        contentValues.put(Schema.TITLE, task.getTaskTitle());
        contentValues.put(Schema.DESCRIPTION, task.getTaskDescription());
        contentValues.put(Schema.CATEGORY, task.getTaskCategory());
        contentValues.put(Schema.COLOR, task.getPriorityColor());
        SQLiteDatabase writeableDatabase = this.getWritableDatabase();
        writeableDatabase.insert(Schema.TABLE_TASKS, Schema.TITLE, contentValues);
        writeableDatabase.close();
    }

    public void insertCategory(String category){

        ContentValues contentValues = new ContentValues();
        contentValues.put(Schema.CATEGORY, category);
        SQLiteDatabase writeableDatabase = this.getWritableDatabase();
        writeableDatabase.insert(Schema.TABLE_CATEGORIES, Schema.CATEGORY, contentValues);
        writeableDatabase.close();
    }

    public ArrayList<Task> getAlltasks(){

        SQLiteDatabase writeableDatabase = this.getWritableDatabase();
        Cursor taskCursor = writeableDatabase.rawQuery(SELECT_ALL_TASKS,null);
        ArrayList<Task> tasks = new ArrayList<>();

        if(taskCursor.moveToFirst()){

            do{

                String title = taskCursor.getString(0);
                String description = taskCursor.getString(1);
                String category = taskCursor.getString(2);
                String color = taskCursor.getString(3);
                tasks.add(new Task(title, description, category, color));

            }while(taskCursor.moveToNext());
        }

        taskCursor.close();
        writeableDatabase.close();
        return tasks;
    }

    public ArrayList<String> getAllCategories(){

        SQLiteDatabase writeableDatabase = this.getWritableDatabase();
        Cursor categoryCursor = writeableDatabase.rawQuery(SELECT_ALL_CATEGORIES,null);
        ArrayList<String> categories = new ArrayList<>();

        if(categoryCursor.moveToFirst()){

            do{

                String category = categoryCursor.getString(0);

                categories.add(category);

            }while(categoryCursor.moveToNext());
        }

        categoryCursor.close();
        writeableDatabase.close();
        return categories;
    }

    public void deleteTask(String title)
    {
        SQLiteDatabase writeableDatabase = this.getWritableDatabase();
        writeableDatabase.delete(Schema.TABLE_TASKS, Schema.TITLE + " = ? ", new String[] {title});
    }

    public static class Schema{

        private static final int SCHEMA_VERSION = 1;
        private static final String DATABASE_NAME = "tasks.db";
        static final String TABLE_TASKS = "TASKS";
        static final String TABLE_CATEGORIES = "CATEGORIES";
        static final String TITLE = "title";
        static final String DESCRIPTION = "description";
        static final String CATEGORY= "category";
        static final String COLOR = "color";

    }
}
