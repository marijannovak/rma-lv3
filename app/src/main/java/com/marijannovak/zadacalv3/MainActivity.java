package com.marijannovak.zadacalv3;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends Activity implements View.OnClickListener {

    ArrayList<Task> taskList;
    ListView lvTasks;
    Button btnAddTask, btnAddCategory;
    TaskAdapter adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        lvTasks = (ListView) findViewById(R.id.lvTaskList);
        btnAddTask = (Button) findViewById(R.id.btnNewTask);
        btnAddCategory = (Button) findViewById(R.id.btnNewCategory);

        btnAddTask.setOnClickListener(this);
        btnAddCategory.setOnClickListener(this);

        taskList = getTaskList();

        if(taskList.isEmpty()) Toast.makeText(this, "No tasks! Add some by pressing the 'New Task' button!", Toast.LENGTH_SHORT).show();


        adapter = new TaskAdapter(taskList);
        lvTasks.setAdapter(adapter);

        lvTasks.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                DatabaseHelper.getInstance(MainActivity.this).deleteTask(taskList.get(position).getTaskTitle());
                adapter.removeAt(position);

                Toast.makeText(MainActivity.this, "Task removed!", Toast.LENGTH_SHORT).show();
                return false;
            }
        });


    }


    @Override
    protected void onResume() {
        super.onResume();

        adapter.notifyDataSetChanged();
    }


    private ArrayList<Task> getTaskList() {

        return DatabaseHelper.getInstance(this).getAlltasks();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btnNewTask:

                Intent newTaskIntent = new Intent(getApplicationContext(), NewTask.class);
                startActivity(newTaskIntent);
                finish();

                break;

            case R.id.btnNewCategory:

                categoryAlert();
                break;
        }
    }

    private void categoryAlert() {


        final EditText txtCategory = new EditText(getApplicationContext());
        txtCategory.setHint("Enter category name");
        txtCategory.setTextColor(Color.BLACK);


        new AlertDialog.Builder(MainActivity.this)
                .setCancelable(true)
                .setTitle("New category")
                .setView(txtCategory)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                        if(txtCategory.getText().toString().isEmpty()) Toast.makeText(MainActivity.this, "You must enter the category title!", Toast.LENGTH_SHORT).show();
                        else {
                            DatabaseHelper.getInstance(MainActivity.this).insertCategory(txtCategory.getText().toString());
                            Toast.makeText(MainActivity.this, "Category added!", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })

                .show();
    }
}
