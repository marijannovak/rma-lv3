package com.marijannovak.zadacalv3;

import java.net.URL;

/**
 * Created by marij on 5.4.2017..
 */

public class Task {

    private String taskTitle;
    private String taskDescription;
    private String taskCategory;
    private String priorityColor;

    public Task (String title, String description, String category, String priority)
    {
        this.taskTitle = title;
        this.taskDescription = description;
        this.taskCategory = category;
        this.priorityColor = priority;

    }


    public String getPriorityColor() {
        return priorityColor;
    }


    public String getTaskTitle() {
        return taskTitle;
    }


    public String getTaskDescription() {
        return taskDescription;
    }


    public String getTaskCategory() {
        return taskCategory;
    }

}
