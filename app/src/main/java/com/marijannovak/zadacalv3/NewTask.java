package com.marijannovak.zadacalv3;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;


public class NewTask extends Activity {

    EditText txtTitle, txtDescription;
    Spinner spCategory, spPriority;
    Button btnCreateTask;
    ArrayList<String> categories, priorities;
    boolean addCategories = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_task);

        txtTitle = (EditText) findViewById(R.id.inputTitle);
        txtDescription = (EditText) findViewById(R.id.inputDescription);
        spCategory = (Spinner) findViewById(R.id.spCategory);
        spPriority = (Spinner) findViewById(R.id.spPriority);
        btnCreateTask = (Button) findViewById(R.id.btnCreate);

        categories = new ArrayList<>();
        priorities = new ArrayList<>();



        categories = DatabaseHelper.getInstance(this).getAllCategories();

        for(String category : categories)
        {
            if(category.equals("Housework")) addCategories = false;
        }

        if(addCategories) {

            for (int i = 0; i < getResources().getStringArray(R.array.categories).length; i++) {

                DatabaseHelper.getInstance(this).insertCategory(getResources().getStringArray(R.array.categories)[i]);
                categories.add(getResources().getStringArray(R.array.categories)[i]);
            }

        }

        for(int i = 0; i < getResources().getStringArray(R.array.priorities).length; i++)
        {
            priorities.add(getResources().getStringArray(R.array.priorities)[i]);
        }


        final ArrayAdapter<String> categoryAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, categories);
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ArrayAdapter<String> priorityAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, priorities );
        priorityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spCategory.setAdapter(categoryAdapter);
        spPriority.setAdapter(priorityAdapter);



        btnCreateTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(txtTitle.getText().toString().isEmpty()) txtTitle.setError("Enter task title!");
                else if(txtDescription.getText().toString().isEmpty()) txtDescription.setError("Enter task description!");
                else
                {
                    Task newTask = new Task(txtTitle.getText().toString(), txtDescription.getText().toString(),
                            spCategory.getSelectedItem().toString(), getPriorityColor());

                    DatabaseHelper.getInstance(NewTask.this).insertTask(newTask);

                    Intent mainIntent = new Intent(NewTask.this, MainActivity.class);
                    startActivity(mainIntent);
                    finish();

                }

            }
        });



    }

    public String getPriorityColor() {

        String color = " ";

        if(spPriority.getSelectedItem().toString().equals("Emergency")) color = "Red";
        else if(spPriority.getSelectedItem().toString().equals("Better hurry!")) color = "Yellow";
        else if(spPriority.getSelectedItem().toString().equals("Casual")) color = "Blue";

        return color;
    }

}
