Ova aplikacija jest To-do lista. Pritiskom na "New Task" dodaje se novi zadatak, tj otvara se novi activity gdje upisujemo
naslov, opis te biramo kategoriju i hitnost zadatka. Zadaci se dodaju u bazu podataka, a iz baze se isčitavaju u listview. 
listview item sadrzi 3 textviewa s naslovom, opisom i kategorijom, te slikom ovisno o kategoriji (College work, housework, job, free time). 
Listview item poprima boju ovisno o odabranom prioritetu (Emergency - crvena, better hurry - zuta, casual - plava). 
Moguce je dodati kategoriju pritiskom na button "New Category". Otvara se alert dialog i trazi unos imena kategorije. Dodaje se 
u bazu podataka pa na izbornik za odabir kategorije zadatka. Ukljucena posebna slika za nove kategorije. Dugim pritiskom na element
listviewa zadatak se brise iz baze i mice sa listviewa.

Najveći problem bio je implementirati bazu podataka. Izvedena je po uzoru na laboratorijske vježbe.

Korištena je biblioteka Picasso za učitavanje slika u ListView. http://square.github.io/picasso/